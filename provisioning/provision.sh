#!/bin/bash

# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD='12345678'
DB_NAME='database'

sudo apt-get upgrade -y
sudo apt-get install -y python-software-properties

php_config_file="/etc/php5/apache2/php.ini"
xdebug_config_file="/etc/php5/mods-available/xdebug.ini"

# Update the server
apt-get update
apt-get -y upgrade

if [[ -e /var/lock/vagrant-provision ]]; then
    exit;
fi

################################################################################
# Everything below this line should only need to be done once
# To re-run full provisioning, delete /var/lock/vagrant-provision and run
#
#    $ vagrant provision
#
# From the host machine
################################################################################

IPADDR=$(/sbin/ifconfig eth0 | awk '/inet / { print $2 }' | sed 's/addr://')
sed -i "s/^${IPADDR}.*//" /etc/hosts
echo $IPADDR ubuntu.localhost >> /etc/hosts			# Just to quiet down some error messages

# Install basic tools
apt-get -y install build-essential binutils-doc git

# Install Apache
apt-get -y install apache2
apt-get -y install php5 php5-curl php5-sqlite php5-xdebug

# MySQL
apt-get -y install php5-mysql phpmyadmin

#MSV
apt-get install php5-intl

sed -i "s/display_startup_errors = Off/display_startup_errors = On/g" ${php_config_file}
sed -i "s/display_errors = Off/display_errors = On/g" ${php_config_file}

cat << EOF > ${xdebug_config_file}
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.remote_host=10.0.2.2
EOF



# Enable mod-rewrite in Apache
a2enmod rewrite
a2enmod ssl

#MSV - TARVITAAN!!!
cat > /etc/apache2/sites-available/000-default.conf <<EOF
DocumentRoot /var/www/html
<Directory />
    Options FollowSymLinks
    AllowOverride All
</Directory>
<Directory "/var/www/html">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order Allow,Deny
    Allow from all
</Directory>
EOF


#MSV
touch /var/www/html/app/logs/error.log

#Needed after CakePHP installation
chown -R www-data /var/www/html/app/tmp
chown -R www-data /var/www/app/logs
chown -R www-data /var/www/html/app/tmp/cache/persistent/



#mkdir -p /etc/apache2/ssl

#Copy files if they do not exist
#rsync -aq /vagrant/provisioning/apache-dev-env-private.key /etc/apache2/ssl/
#rsync -aq /vagrant/provisioning/apache-dev-env.crt /etc/apache2/ssl/
#rsync -aq /vagrant/provisioning/default-ssl.conf /etc/apache2/sites-available/

#sudo a2ensite default-ssl.conf

# Restart Services
service apache2 restart

# Cleanup the default HTML file created by Apache
#if [ -f "/var/www/html/index.html" ]; then
	rm /var/www/html/index.html
#if

# Restart Services
service apache2 restart

# Cleanup the default HTML file created by Apache
rm /var/www/html/index.html

touch /var/lock/vagrant-provision
