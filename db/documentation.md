# Using Liquibase

## Introduction
Liquibase is a VCS (Version Control System) for databases, unlike git using Liquibase is
really manual (perhaps in the future will be worth use it alongside maven or ant). Instead of making
commits we use changelog files where the database changes are wrote in xml format.

Every changelog file has a number of "\<changeSet\>" tags which represent every single change, 
it is recommended to keep them small so that you can discover bugs by rolling back in case of
database failure.



## changeSet
Every changeSet has to have an id and an author for example:
```xml
<changeSet id="1" author="alvaro">
        <comment>A sample change log</comment>
        <createTable tableName="example">
            <column name="id" type="serial">
                <constraints primaryKey="true" nullable="false" unique="true"/>
            </column>
        </createTable>
    </changeSet>
```
The id has to be unique for each author, and it could be a number or a string. [See more here](http://www.liquibase.org/documentation/changeset.html)

### About "insert" data
It seems that changes in schema are revertible but **not** changes in the data. If you made a changeSet which inserts
date to one or more tables you must write a \<rollback\> tag of how to revert the changes. Please read [this](http://www.liquibase.org/documentation/rollback.html)

## ChangeLog 
your changeSets go inside of a ChangeLog `db.changelog-version_number.xm` File which must be referenced in db.changelog-master.xml like this
```xml
<include file="changelog/db.changelog-version_number.xml"/>
```

## Commands
Since liquibase needs to access the database you must run it from the Virtual machine so issue `vagrant ssh`
and `cd /vagrant/db` to access the folder.

- to execute liquibase and add all the changes to your database just do:  `./liquibase update`

- move backwards you may do `rollbackCount n`  where *n* is the number of resutSets you want go backwards

All liquibase commands [here](http://www.liquibase.org/documentation/command_line.html)


## Prerequisites 
- Install java in your virtual machine if necessary `sudo apt-get install default-jre`
- the software is in this repository so no need to download anything else.

## Recommendations
Looks like Netbeans has autocompletion for liquibase xml tags making it much easier to deal to.

## Example and template
- use the changelog version 1.0 as a template
- see the example file.

## Documentation
[go here](http://www.liquibase.org/documentation/) <br/>
Use the search to find the xml tags in the  documentation 