# Word Practice Project
## Installation of CakePHP framework
- Install composer file from `curl -sS https://getcomposer.org/installer | php`
- set the `composer.json` file.
- Issue `php composer.phar install` to install CakePHP
- `mkdir app`
- Issue `Vendor/bin/cake bake project app` to generate the application (in the folder project)
- Rename database.php.default to database.php in /Config then fill database user, password and name of database
### DebugKit
- Activate DebugKit plugin in /Config/bootstrap.php (instructions in the file)
- Add `var $components = array('DebugKit.Toolbar');` in Controller/AppController.php
### Configure netbeans cakePHP extension
![hi](add-this.PNG)

