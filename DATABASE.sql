use wordpractice;

CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    language VARCHAR(50),
    created DATETIME,
    modified DATETIME,
    UNIQUE KEY (email)
);

CREATE TABLE IF NOT EXISTS words (
	id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    name VARCHAR(50),
    from_lang VARCHAR(50),
    to_lang VARCHAR(50),
    meaning1 VARCHAR(50),
    meaning2 VARCHAR(50),
    UNIQUE KEY (name),
    FOREIGN KEY user_key (user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS scores (
	id INT AUTO_INCREMENT PRIMARY KEY,
	user_id INT NOT NULL,
    total_practices INT,
    successes INT,
    FOREIGN KEY user_key (user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS languages (
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    code VARCHAR(50),
    UNIQUE KEY (name)
);

-- Populate tables 

INSERT IGNORE INTO languages values 
(1, 'English', 'EN'),
(2, 'Spanish', 'ES'),
(3, 'Finnish', 'FI');
