<div class="words index">
	<h2><?php echo __('Words'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('from_lang'); ?></th>
			<th><?php echo $this->Paginator->sort('to_lang'); ?></th>
			<th><?php echo $this->Paginator->sort('meaning1'); ?></th>
			<th><?php echo $this->Paginator->sort('meaning2'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($words as $word): ?>
	<tr>
		<td><?php echo h($word['Word']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($word['User']['id'], array('controller' => 'users', 'action' => 'view', $word['User']['id'])); ?>
		</td>
		<td><?php echo h($word['Word']['name']); ?>&nbsp;</td>
		<td><?php echo h($word['Word']['from_lang']); ?>&nbsp;</td>
		<td><?php echo h($word['Word']['to_lang']); ?>&nbsp;</td>
		<td><?php echo h($word['Word']['meaning1']); ?>&nbsp;</td>
		<td><?php echo h($word['Word']['meaning2']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $word['Word']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $word['Word']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $word['Word']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $word['Word']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
        <div>
            <h3>Spanish words</h3>
            <p>
                <?php foreach($SpanishWords as $w):?>
                <?php echo $w['Word']['name'] . ' '?>
                <?php endforeach;?>
            </p>
        </div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Word'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
