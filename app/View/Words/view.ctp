<div class="words view">
<h2><?php echo __('Word'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($word['Word']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($word['User']['id'], array('controller' => 'users', 'action' => 'view', $word['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($word['Word']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('From Lang'); ?></dt>
		<dd>
			<?php echo h($word['Word']['from_lang']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('To Lang'); ?></dt>
		<dd>
			<?php echo h($word['Word']['to_lang']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meaning1'); ?></dt>
		<dd>
			<?php echo h($word['Word']['meaning1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meaning2'); ?></dt>
		<dd>
			<?php echo h($word['Word']['meaning2']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Word'), array('action' => 'edit', $word['Word']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Word'), array('action' => 'delete', $word['Word']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $word['Word']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Words'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Word'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
