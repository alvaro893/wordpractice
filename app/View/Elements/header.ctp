<style>
    nav a {
        color: white;
    }
</style>

<div class="masthead" >
    <nav >
        <a href="#">Home</a>
        <?php echo $this->Html->link(__('Sign up'), '/users/add', ['class' => 'button']) ?>
        <?php echo $this->Html->link('Words', array('controller' => 'words', 'action' =>'index')); ?>
        <?php echo $this->Html->link('game', array('controller' => 'gameTables', 'action' => 'game') )?>
    </nav>
    <h3 class="muted">Word Practice</h3>
</div>
<hr>