<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Score $Score
 * @property Word $Word
 */
class GameTable extends AppModel {
    public $hasOne = 'User';
    public $hasMany = 'Word';
    
}